import React from 'react'
import './Header.css'
import logo from '../../photo/logo.svg'
import logo__basket from '../../photo/logo__basket.svg'
import { Link } from 'react-router-dom'

const AppHeader = () => {
    return (
        <div>
        <div className='App-header'>
            <div className='header__section'>
                <div className='header__item headerlogo'>
                    <a href='https://google.com'><img src={logo} alt='' /></a>
                </div>
                <span className='header__item '><Link to='/catalog' className='header_src'>Каталог</Link></span>
                <span className='header__item '><Link to='/' className='header_src' >Бренды</Link></span>
                <span className='header__item '><Link to='/' className='header_src' >О нас</Link></span>
            </div>
            <div className='header__section'>
                <div className='header__item'>
                    <form className='header__item input__search'>
                        <input type='text' className=' search logo__loupe'/>
                    </form>
                </div>
                <div className='header__item'>
                    <Link to='/basket' className="down_header_src logo__basket" >
                        <img src={logo__basket} alt=''  /></Link>
                </div>
            </div>
        </div>
                <div className="menu">
                    <Link to="/" className='down_header_src'>Home</Link>
                    <Link to="/catalog" className='down_header_src' >Catalog</Link>
                    <Link to="/details" className="down_header_src Link">Детали продукта</Link>
                </div>
        </div>



    )
}
export default AppHeader