import React from 'react'
import './Home-page.css'
import back from './foto/back.png'
import side_cap from './foto/side_cap.png'
import diagonal from './foto/diagonal.png'
import ellipse_red from './foto/ellipse_red.svg'
import ellipse_yellow from './foto/ellipse_yellow.svg'
import ellipse_white from './foto/ellipse_white.svg'
import red from "./foto/red.svg";
import black from "./foto/black.svg";
import violet from "./foto/violet.svg";
import vector_right from "./foto/vector_right.svg";
import mini_white from "./foto/mini_white.png";
import mini_red from "./foto/mini_red.png";
import mini_blue from "./foto/mini_blue.png";
import vector_left from "./foto/vector_left.svg";
import supreme_black from "./foto/supreme_black.svg";
import supreme_floral from "./foto/supreme_floral.svg";
import supreme_red from "./foto/supreme_red.svg";
import tree_caps from "./foto/tree_caps.png";
import ellipse_violet from "./foto/ellipse_violet.png";
import { Link } from 'react-router-dom'

const HomePage = () => {
    return (
        <div className='page'>
            <div className="firstSection">
                <div className='content_text'>
                    <div className="yellow">
                        <img src={ellipse_yellow} alt=""/>
                    </div>
                    <div className="diagonal">
                        <img src={side_cap} alt=""/>
                    </div>
                    <div className="back">
                        <img src={back} alt="" />
                    </div>
                    <div className="red">
                        <img src={ellipse_red} alt=""/>
                    </div>
                    <div className="ellipse_white">
                        <img src={ellipse_white} alt=""/>
                    </div>
                    <h1> New Era <div className='dot'></div></h1>
                    <div className="description">
                        <p> Новая коллекция 2021 уже доступны на заказ
                            яркие цвета, винтажный принт 70х, тематические
                            группы и отличное качество </p>
                    </div>
                    <Link to='/catalog' className='down_header_src src_home'><button className="btn-yellow">Открыть каталог</button></Link>
                    <div className="side">
                        <img src={diagonal} alt=""/>
                    </div>
                </div>
            </div>

            <div className='second_section'>
                <div className='wrapper2'>
                <div className='recommendations_block'>
                    <div className='recommendations'>
                        <div className='mini'>
                            <div className='img'>
                                <img src={red} alt=''/>
                            </div>
                            <div className='mini_info'>
                                <span>2021</span>
                                <h3>New Era</h3>
                                <p className='test'>Houston Rockets</p>
                                <div className='price_sale'>2399c</div>
                            </div>

                        </div>
                    </div>

                    <div className='recommendations'>
                        <div className='mini'>
                            <div className='img'>
                                <img src={black} alt=''/>
                            </div>
                            <div className='mini_info'>
                                <p>2021</p>
                                <h3>New Era</h3>
                                <p className='test'>Chicago WhiteSox</p>
                                <div className='price_sale'>2399c</div>
                            </div>

                        </div>
                    </div>

                    <div className='recommendations'>
                        <div className='mini'>
                            <div className='img'>
                                <img src={violet} alt=''/>
                            </div>
                            <div className='mini_info'>
                                <p>2021</p>
                                <h3>New Era</h3>
                                <p className='test'>LA Lakers</p>
                                <div className='price_sale'>2399c</div>
                            </div>

                        </div>
                    </div>
                </div>

                <div className='title'>
                    <h1> TOP SELLERS <div className='mini_dot'></div></h1>
                </div>
                <div className='slider'>
                    <div className='box-brand'>
                        <div className='slider_1'>

                            <div className='Photos'>
                                <div className='vector'>
                                    <img src={vector_right} alt=""/>
                                </div>
                                <div className='mini_white'>
                                    <img src={mini_white} alt=""/>
                                    <div className='text'>
                                        <h3>Adidas</h3>
                                        <p>San Francisco Baseball</p>
                                        <div className='price_cap'>3800c</div>
                                    </div>
                                </div>
                                <div className='mini_red'>
                                    <img src={mini_red} alt=""/>
                                    <div className='text'>
                                        <h3>New Era</h3>
                                        <p>New York Yankies</p>
                                        <div className='price_cap'>3800c</div>
                                    </div>
                                </div>
                                <div className='mini_blue'>
                                    <img src={mini_blue} alt=""/>
                                    <div className='text'>
                                        <h3>Nike</h3>
                                        <p>French Fries Series</p>
                                        <div className='price_cap'>4500c</div>
                                    </div>

                                </div>
                                <div className='vector'>
                                    <img src={vector_left} alt=''/>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
                </div>

            <div className='third_section'>
                    <div className='wrapper3'>
                    <div className='block'>
                        <div className='content'>
                            <div className='left_content'>
                                <div className='mini_block'>
                                    <div className='recommendation'>
                                        <div className='informationOfCap'>
                                            <h5 className='name_cap'>Supreme</h5>
                                            <p>Liberty</p>
                                            <h5 className='priceCap'>5600c</h5>
                                        </div>
                                    <div className='cap_photo'>
                                            <img src={supreme_black} alt=''/>
                                    </div>
                                    </div>
                                </div>
                                <div className='mini_block'>
                                    <div className='recommendation'>
                                    <div className='informationOfCap'>
                                            <h5 className='name_cap'>Supreme</h5>
                                            <p>Floral</p>
                                            <h5 className='priceCap'>5600c</h5>
                                    </div>
                                    <div className='cap_photo'>
                                            <img src={supreme_floral} alt=''/>
                                    </div>
                                    </div>
                                </div>
                                <div className='mini_block'>
                                    <div className='recommendation'>
                                    <div className='informationOfCap'>
                                            <h5 className='name_cap'>Supreme</h5>
                                            <p>Playboy</p>
                                            <h5 className='priceCap'>5600c</h5>
                                    </div>
                                    <div className='cap_photo'>
                                            <img src={supreme_red} alt='' />
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div className='middle_content'>
                                <div className='text_content'>
                                    <div className='collaboration'>
                                        <h1>SUPREME & NEW ERA</h1>
                                        <p>collaboration</p>
                                        <button className='btn_violet'>Открыть каталог</button>
                                    </div>
                                    <div className='big_img1 '>
                                        <img src={tree_caps} alt='' />
                                </div>
                                </div>
                            </div>
                            <div className="ellipse_violet">
                                <img src={ellipse_violet} alt=""/>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        
            <div className='fourth_section'>
                <div className='wrapper4'>
                    <div className='name_number'>CUSTOM CUPS в цифрах</div>
                    <div className='result'>
                        <div className="numbers-item">
                            <p><h1>12000</h1>Проданных кепок</p>
                        </div>
                        <div className="numbers-item">
                            <p><h1>9</h1>лет на рынке</p>
                        </div>
                        <div className="numbers-item">
                            <p><h1>8500</h1>довольных клиентов</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        
    )
}
export default HomePage