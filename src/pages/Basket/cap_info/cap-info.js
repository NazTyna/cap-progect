import React from 'react'
import './cap-info.css'
import cap_img from '../photo/cap.png'

const CapInfo = () => {
    return (
        <div className='wrapper'>
            <div className='basket'>
                
                <div className='wrapper__cap'>
                    <div className='trait'></div>
                    <div className='cap-info'>
                        <div className='cap_img'>
                            <img src={cap_img} alt='cap' />
                        </div>
                        <div className='counter_cap'>
                            <div className='minus black'>-</div>
                            <div className='count'>1</div>
                            <div className='plus black'>+</div>
                        </div>
                        <div className='size'>
                            <button className='adapter active'>L</button>
                        </div>
                        <div className='name_cap'>
                            <h3>NEW ERA</h3>
                            <p>BLACK SNAPBACK 59 FIFTY</p>
                        </div>
                        <div className='price_cap'>
                            <h4>3200сом</h4>
                        </div>
                    </div>
                    <div className='trait'></div>
                </div>
                <div className='wrapper__info'>
                    <div className='input_info'>
                        <h2>Ваша информация</h2>
                        <div className='inputs'>
                            <input type='text' placeholder='Имя' className='input name'/>
                            {/* <input type='text' placeholder='Фамилия' className='input surname'/> */}
                            <input type='number' placeholder='+996 777 888 999' className='input number'/>
                            {/* <input type='text' placeholder='Email' className='input email'/> */}
                            {/* <input type='text' placeholder='Адрес' className='input adress'/> */}
                        </div>
                    </div>
                    {/* <div className='trait'></div> */}
                    <div className='buy'>
                        <button className='btn btn-yellow'>Купить</button>
                    </div>
                    
                </div>

            </div>
        </div>
    )
}

export default CapInfo